import Redis from 'ioredis';

let client;

/**
 *
 * @type {{STR: string, SETS: string, OBJECT: string, LIST: string}}
 */
export const REDIS_ENUM = {
    LIST: 'list',
    SETS: 'sets',
    OBJECT: 'object',
    STR: 'str',
};

/**
 *
 * @param config
 * @constructor
 */
function RedisInit(config) {
    client = new Redis(config);
    client.on('connect', () => {
        console.log(`>>> RedisClient connected ${config.db ? `to ${config.db}` : ''}`);
    });
    client.on('error', (e) => {
        console.log('>>> RedisClient error connected!', e.message);
    });
}

/**
 *
 * @param key
 * @param type {{STR: string, SETS: string, OBJECT: string, LIST: string}}
 * @returns {Promise<string[]|Record<string, string>|*>}
 */
export async function getValue(key, type) {
    switch (type) {
        case REDIS_ENUM.LIST:
            return client.lrange(key, 0, -1);
        case REDIS_ENUM.SETS:
            return client.smembers(key);
        case REDIS_ENUM.OBJECT:
            return client.hgetall(key);
        case REDIS_ENUM.STR:
        default:
            return client.get(key);
    }
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
export function setListValue(key, value, expiration) {
    client.rpush(key, value);
    if (expiration) {
        client.expire(key, expiration);
    }
}

/**
 *
 * @param key
 * @param values
 * @param expiration
 */
export function setUniqueListValues(key, values, expiration) {
    values.unshift(key);

    client.sadd(values);
    if (expiration) {
        client.expire(key, expiration);
    }
}

/**
 *
 * @param key
 * @param value
 * @returns {Result<number, {type: "default"}>}
 */
export function removeValueFromList(key, value) {
    return client.lrem(key, 0, value);
}

/**
 *
 * @param key
 * @returns {Result<string | null, {type: "default"}> | Result<string[] | null, {type: "default"}>}
 */
export function removeLastValueFromList(key) {
    return client.rpop(key);
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
export function setStrValue(key, value, expiration) {
    client.set(key, value);
    if (expiration) {
        client.expire(key, expiration);
    }
}

/**
 *
 * @param key
 * @param data
 * @param expiration
 */
export function setObjectValue(keys, data, expiration) {
    if (Array.isArray(keys)) {
        keys.forEach((key) => {
            client.hmset(key, data);

            if (expiration) {
                client.expire(key, expiration);
            }
        });
        return;
    }

    client.hmset(keys, data);
    if (expiration) {
        client.expire(keys, expiration);
    }
}

/**
 *
 * @param key
 * @param field
 * @param inc
 */
export function setIncrObjectValue(key, field, inc) {
    client.hincrby(key, field, inc);
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
export function setIncrValue(key, value, expiration) {
    client.incrby(key, value);
    if (expiration) {
        client.expire(key, expiration);
    }
}

/**
 *
 * @param key
 */
export function deleteKey(key) {
    client.del(key);
}

export default RedisInit;
