import amqplib from 'amqplib';
import _ from 'lodash';
import EventEmitter from 'events';
import { v4 as uuidv4 } from 'uuid';

let instance;

class RmqClient {
  constructor() {
    this.queues = {};
  }

  /**
   *
   * @returns {Promise<RmqClient>}
   */
  async init(url) {
    try {
      this.connection = await amqplib.connect(url);
      this.channel = await this.connection.createChannel();
      this.channel.responseEmitter = new EventEmitter();
      this.channel.responseEmitter.setMaxListeners(0);

      this.channel.consume(
        'amq.rabbitmq.reply-to',
        (msg) => {
          this.channel.responseEmitter.emit(msg.properties.correlationId, JSON.parse(msg.content.toString()));
        },
        { noAck: true },
      );

      console.log('>>> RmqClient connected!');

      return this;
    } catch (e) {
      console.log('>>> RmqClient ERROR!', e.message);
      throw e;
    }
  }

  /**
   * @param ex
   * @param routingKey
   * @param msg
   * @returns {Promise<void>}
   */
  async publish({ ex, routingKey }, msg) {
    const queue = `${ex}.${routingKey}`;
    await this.channel.assertQueue(queue, { durable: true });
    this.channel.bindQueue(queue, ex, routingKey);
    this.channel.publish(ex, routingKey, Buffer.from(msg), {
      messageId: uuidv4(),
    });
  }

  /**
   *
   * @param ex
   * @param routingKey
   * @param messageId
   * @param msg
   * @param resolve
   */
  send({ ex, routingKey, messageId = uuidv4(), expiration }, msg, resolve, reject) {
    const queue = `${ex}.${routingKey}`;
    const correlationId = uuidv4();
    let isRead = false;

    const rejectTimeout = expiration && setTimeout(() => {
      if (!isRead) {
        reject(new Error('Microservice does not answer!'));
      }
      return clearTimeout(rejectTimeout);
    }, expiration);

    this.channel.responseEmitter.once(correlationId, (data) => {
      resolve(data);
      isRead = true;
      clearTimeout(rejectTimeout);
    });

    this.channel.sendToQueue(queue, Buffer.from(msg), {
      correlationId,
      replyTo: 'amq.rabbitmq.reply-to',
      messageId,
      expiration,
    });
  }

  /**
   * @param exchange
   * @param bindingKey
   * @param handler
   * @returns {Promise<function(): Promise<void>>}
   */
  async subscribe({ exchange, bindingKey }, handler) {
    const queue = `${exchange}.${bindingKey}`;
    if (!this.connection) {
      await this.init();
    }

    await this.channel.assertExchange(exchange, 'direct', { durable: true });

    if (this.queues[queue]) {
      const existingHandler = _.find(this.queues[queue], (h) => h === handler);
      if (existingHandler) {
        return () => this.unsubscribe(queue, existingHandler);
      }
      this.queues[queue].push(handler);
      return () => this.unsubscribe(queue, handler);
    }

    await this.channel.assertQueue(queue, { durable: true });
    this.channel.bindQueue(queue, exchange, bindingKey);
    this.queues[queue] = [handler];
    this.channel.consume(queue, async (msg) => {
      const ack = _.once(() => this.channel.ack(msg));
      this.queues[queue].forEach((h) => h(msg, ack));
    });
    return () => this.unsubscribe(queue, handler);
  }

  /**
   * @param queue
   * @param handler
   * @returns {Promise<void>}
   */
  async unsubscribe(queue, handler) {
    _.pull(this.queues[queue], handler);
  }
}

/**
 * Singleton
 * @returns {Promise<RmqClient>}
 */
RmqClient.getInstance = async function (url) {
  if (!instance) {
    const broker = new RmqClient();
    instance = broker.init(url);
  }
  return instance;
};

/**
 *
 * @param url RMQ URL CONNECTION
 * @param events
 *
 * events = [
 *    {
 *      queue: exchange который слушаем;
 *      event: тип ивента;
 *      callback: (payload, closeQueue) => void;
 *        payload = {
 *            data: сообщение, любой тим данных;
 *            properties: если сообщение ожидает обратного ответа - присылаем replyTo и correlationId любо undefined;
 *       },
 *       closeQueue: коллбек закрытия очереди по завершению логики или ошибки;
 *    }
 * ]
 */
export function onEventListeners(url, events) {
  RmqClient.getInstance(url).then((broker) => {
    function onEventListen(queue, event, callback) {
      let msgId;

      broker.subscribe({ exchange: queue, bindingKey: event }, (data, closeQueue) => {
        const { replyTo, correlationId, messageId } = data.properties;
        console.log(`>>> Subscription RMQ ${queue}.${event} ${JSON.stringify(JSON.parse(data.content))}\n`, JSON.stringify({
          replyTo,
          correlationId,
          messageId,
        }));

        if (msgId === messageId) {
          console.log('>>> Message with this id has already been processed');
          return closeQueue();
        }

        msgId = messageId;

        callback(
            {
              data: JSON.parse(data.content),
              properties: replyTo && correlationId && { replyTo, correlationId },
            },
            closeQueue,
        );
      });
    }

    if (!Array.isArray(events)) {
      throw new Error('onEventListeners expects an array of rabbit connections');
    }
    events.forEach((item) => {
      const { queue, event, callback } = item;
      onEventListen(queue, event, callback);
    });
  });
}

/**
 *
 * @param url
 * @param queue
 * @param event
 * @param payload
 * @returns {Promise<void>}
 */
export async function emitEvent(url, { queue, event, payload }) {
  const broker = await RmqClient.getInstance(url);
  await broker.publish({ ex: queue, routingKey: event }, JSON.stringify(payload));
  console.log(`>>> Emit RMQ ${queue}.${event} \n`, JSON.stringify(payload));
}

/**
 *
 * @param url
 * @param queue
 * @param event
 * @param payload
 * @param messageId
 * @param expiration
 * @returns {Promise<void>}
 */
export function sendEventMessage(url, { queue, event, payload, messageId, expiration }) {
  return new Promise((resolve, reject) => {
    RmqClient.getInstance(url).then((broker) => {
      broker.send({ ex: queue, routingKey: event, messageId, expiration }, JSON.stringify(payload), resolve, reject);
      console.log(`>>> Send message RMQ ${queue}.${event} \n`, JSON.stringify(payload));
    });
  });
}

/**
 *
 * @param replyTo
 * @param correlationId
 * @param response
 * @returns {Promise<void>}
 */
export async function sendResponse(replyTo, correlationId, response) {
  const broker = await RmqClient.getInstance();
  broker.channel.sendToQueue(replyTo, Buffer.from(JSON.stringify(response)), { correlationId });
  console.log(`>>> Send response RMQ to ${correlationId} -> ${replyTo} \n`, JSON.stringify(response));
}
