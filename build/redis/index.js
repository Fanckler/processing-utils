"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.REDIS_ENUM = void 0;
exports.deleteKey = deleteKey;
exports.getValue = getValue;
exports.removeLastValueFromList = removeLastValueFromList;
exports.removeValueFromList = removeValueFromList;
exports.setIncrObjectValue = setIncrObjectValue;
exports.setIncrValue = setIncrValue;
exports.setListValue = setListValue;
exports.setObjectValue = setObjectValue;
exports.setStrValue = setStrValue;
exports.setUniqueListValues = setUniqueListValues;
var _ioredis = _interopRequireDefault(require("ioredis"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
let client;

/**
 *
 * @type {{STR: string, SETS: string, OBJECT: string, LIST: string}}
 */
const REDIS_ENUM = {
  LIST: 'list',
  SETS: 'sets',
  OBJECT: 'object',
  STR: 'str'
};

/**
 *
 * @param config
 * @constructor
 */
exports.REDIS_ENUM = REDIS_ENUM;
function RedisInit(config) {
  client = new _ioredis.default(config);
  client.on('connect', () => {
    console.log(`>>> RedisClient connected ${config.db ? `to ${config.db}` : ''}`);
  });
  client.on('error', e => {
    console.log('>>> RedisClient error connected!', e.message);
  });
}

/**
 *
 * @param key
 * @param type {{STR: string, SETS: string, OBJECT: string, LIST: string}}
 * @returns {Promise<string[]|Record<string, string>|*>}
 */
async function getValue(key, type) {
  switch (type) {
    case REDIS_ENUM.LIST:
      return client.lrange(key, 0, -1);
    case REDIS_ENUM.SETS:
      return client.smembers(key);
    case REDIS_ENUM.OBJECT:
      return client.hgetall(key);
    case REDIS_ENUM.STR:
    default:
      return client.get(key);
  }
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
function setListValue(key, value, expiration) {
  client.rpush(key, value);
  if (expiration) {
    client.expire(key, expiration);
  }
}

/**
 *
 * @param key
 * @param values
 * @param expiration
 */
function setUniqueListValues(key, values, expiration) {
  values.unshift(key);
  client.sadd(values);
  if (expiration) {
    client.expire(key, expiration);
  }
}

/**
 *
 * @param key
 * @param value
 * @returns {Result<number, {type: "default"}>}
 */
function removeValueFromList(key, value) {
  return client.lrem(key, 0, value);
}

/**
 *
 * @param key
 * @returns {Result<string | null, {type: "default"}> | Result<string[] | null, {type: "default"}>}
 */
function removeLastValueFromList(key) {
  return client.rpop(key);
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
function setStrValue(key, value, expiration) {
  client.set(key, value);
  if (expiration) {
    client.expire(key, expiration);
  }
}

/**
 *
 * @param key
 * @param data
 * @param expiration
 */
function setObjectValue(keys, data, expiration) {
  if (Array.isArray(keys)) {
    keys.forEach(key => {
      client.hmset(key, data);
      if (expiration) {
        client.expire(key, expiration);
      }
    });
    return;
  }
  client.hmset(keys, data);
  if (expiration) {
    client.expire(keys, expiration);
  }
}

/**
 *
 * @param key
 * @param field
 * @param inc
 */
function setIncrObjectValue(key, field, inc) {
  client.hincrby(key, field, inc);
}

/**
 *
 * @param key
 * @param value
 * @param expiration
 */
function setIncrValue(key, value, expiration) {
  client.incrby(key, value);
  if (expiration) {
    client.expire(key, expiration);
  }
}

/**
 *
 * @param key
 */
function deleteKey(key) {
  client.del(key);
}
var _default = RedisInit;
exports.default = _default;